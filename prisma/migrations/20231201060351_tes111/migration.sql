/*
  Warnings:

  - Added the required column `npm` to the `Member` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `Member` ADD COLUMN `npm` VARCHAR(45) NOT NULL;
