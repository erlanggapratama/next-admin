-- CreateTable
CREATE TABLE `Member` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `nama` VARCHAR(45) NULL,
    `jabatan_id` INTEGER NULL,
    `email` VARCHAR(45) NULL,
    `alamat` VARCHAR(45) NULL,
    `tempatlahir` VARCHAR(45) NULL,
    `tanggallahir` VARCHAR(45) NULL,
    `nohp` VARCHAR(45) NULL,
    `otw_nama` VARCHAR(45) NULL,
    `otw_nohp` VARCHAR(45) NULL,
    `status_alumni` INTEGER NULL,
    `status_pelatih` INTEGER NULL,
    `valid` INTEGER NULL,

    INDEX `Jabatan_id_idx`(`jabatan_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Member` ADD CONSTRAINT `id` FOREIGN KEY (`jabatan_id`) REFERENCES `Jabatan`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
