FROM node:18.17.0-alpine as builder
RUN apk add --no-cache libc6-compat
WORKDIR /nextadmin
COPY package.json package-lock.json ./
RUN npm cache clear --force
RUN npm install --legacy-peer-deps
COPY . .
ENV NEXT_TELEMETRY_DISABLED 1
RUN npx prisma migrate deploy
RUN npx prisma generate
RUN npm run build

FROM node:18.17.0-alpine as runner
WORKDIR /nextadmin
ENV NODE_ENV production
ENV NEXT_TELEMETRY_DISABLED 1
RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs
COPY --from=builder /nextadmin/package.json .
COPY --from=builder /nextadmin/package-lock.json .
COPY --from=builder /nextadmin/next.config.js ./
COPY --from=builder /nextadmin/public ./public
COPY --from=builder /nextadmin/.next/standalone ./
COPY --from=builder /nextadmin/.next/static ./.next/static
COPY --from=builder --chown=nextjs:nodejs /nextadmin/.next/standalone ./
COPY --from=builder --chown=nextjs:nodejs /nextadmin/.next/static ./.next/static
COPY --chown=nextjs:nodejs prisma ./prisma/
USER nextjs
EXPOSE 3000
ENV PORT 3000
ENTRYPOINT ["npm", "start"]