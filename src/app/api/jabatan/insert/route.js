import prisma from "@/utils/prisma";

export async function POST(request) {
  try {
    const value = await request.json();
    const createMember = await prisma.jabatan.create({
      data: {
        nama: value.nama,
      },
    });
    if (createMember) {
      return Response.json({ message: "Sucess" }, { status: 200 });
    }
    return Response.json({}, { status: 400 });
  } catch (e) {
    console.log(e);
    return Response.json({}, { status: 400 });
  }
}
