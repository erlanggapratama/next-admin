import prisma from "@/utils/prisma";

export async function POST(request) {
  try {
    const value = await request.json();
    const updateJabatan = await prisma.jabatan.update({
      where: {
        id: value.id,
      },
      data: {
        nama: value.nama,
      },
    });
    if (updateJabatan) {
      return Response.json({ message: "Sucess" }, { status: 200 });
    }
    return Response.json({}, { status: 400 });
  } catch (e) {
    return Response.json({}, { status: 400 });
  }
}
