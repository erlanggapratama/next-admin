import prisma from "@/utils/prisma";

export async function GET() {
  try {
    const checkUser = await prisma.jabatan.findMany({
      select: {
        id: true,
        nama: true,
      },
    });
    return Response.json(
      {
        message: "success",
        data: checkUser,
      },
      { status: 200 }
    );
  } catch (e) {
    console.log(e);
    return Response.json({}, { status: 400 });
  }
}
