import prisma from "@/utils/prisma";

export async function POST(request) {
  try {
    const value = await request.json();
    delete value.jabatan_id;
    const createUser = await prisma.member.create({
      data: {
        ...value,
      },
    });
    if (createUser) {
      return Response.json({ message: "Sucess" }, { status: 200 });
    }
    return Response.json({}, { status: 400 });
  } catch (e) {
    console.log("err", e);
    return Response.json({}, { status: 400 });
  }
}
