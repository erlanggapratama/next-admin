import prisma from "@/utils/prisma";

export async function GET() {
  try {
    const checkUser = await prisma.user.findMany({
      select: {
        id: true,
        username: true,
        title: true,
      },
    });
    return Response.json(
      {
        message: "success",
        data: checkUser,
      },
      { status: 200 }
    );
  } catch (e) {
    return Response.json({}, { status: 400 });
  }
}
