import prisma from "@/utils/prisma";

export async function POST(request) {
  try {
    const value = await request.json();
    const updateUser = await prisma.user.update({
      where: {
        id: value.id,
      },
      data: {
        username: value.username,
        title: value.title,
      },
    });
    if (updateUser) {
      return Response.json({ message: "Sucess" }, { status: 200 });
    }
    return Response.json({}, { status: 400 });
  } catch (e) {
    return Response.json({}, { status: 400 });
  }
}
