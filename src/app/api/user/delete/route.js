import prisma from "@/utils/prisma";

export async function POST(request) {
  try {
    const value = await request.json();
    const deleteUser = await prisma.user.delete({
      where: {
        id: value.id,
      },
    });
    if (deleteUser) {
      return Response.json({ message: "Sucess" }, { status: 200 });
    }
    return Response.json({}, { status: 400 });
  } catch (e) {
    return Response.json({}, { status: 400 });
  }
}
