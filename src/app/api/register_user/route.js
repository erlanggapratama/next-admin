import * as bcyrpt from "bcrypt";
import prisma from "@/utils/prisma";

export async function POST(request) {
  try {
    const value = await request.json();
    const encryptPass = await bcyrpt.hash(value.password, 12);
    const createUser = await prisma.user.create({
      data: {
        username: value.username,
        title: value.title,
        password: encryptPass,
      },
    });
    if (createUser) {
      return Response.json({ message: "Sucess" }, { status: 200 });
    }
    return Response.json({ status: 400 });
  } catch (e) {
    return Response.json({ status: 400 });
  }
}
