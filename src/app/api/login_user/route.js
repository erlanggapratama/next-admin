import * as bcyrpt from "bcrypt";
import prisma from "@/utils/prisma";

export async function POST(request) {
  try {
    const value = await request.json();
    const checkUser = await prisma.user.findFirst({
      where: {
        username: value.username,
      },
    });
    if (checkUser) {
      const checkpass = await bcyrpt.compare(
        value.password,
        checkUser.password
      );

      if (checkpass) {
        return Response.json(
          {
            message: "success",
            data: { username: checkUser.username, title: checkUser.title },
          },
          { status: 200 }
        );
      }
    }
    return Response.json({ status: 400 });
  } catch (e) {
    return Response.json({ status: 400 });
  }
}
