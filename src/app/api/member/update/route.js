import prisma from "@/utils/prisma";

export async function POST(request) {
  try {
    const value = await request.json();
    delete value.jabatan_id;
    const { id, ...restValue } = value;
    const updateMember = await prisma.member.update({
      where: {
        id,
      },
      data: {
        ...restValue,
      },
    });
    if (updateMember) {
      return Response.json({ message: "Sucess" }, { status: 200 });
    }
    return Response.json({}, { status: 400 });
  } catch (e) {
    return Response.json({}, { status: 400 });
  }
}
