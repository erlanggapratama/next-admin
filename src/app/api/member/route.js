import prisma from "@/utils/prisma";

export async function GET() {
  try {
    const checkMember = await prisma.member.findMany({
      include: {
        Jabatan: true,
      },
    });
    if (checkMember)
      return Response.json(
        {
          message: "success",
          data: checkMember,
        },
        { status: 200 }
      );

    return Response.json({}, { status: 400 });
  } catch (e) {
    return Response.json({}, { status: 400 });
  }
}
