"use client";
import { observer, useLocalObservable } from "mobx-react-lite";
import { action, runInAction } from "mobx";
import {
  Box,
  Typography,
  TextField,
  Button,
  FormHelperText,
  Select,
  InputLabel,
  FormControl,
  MenuItem,
  IconButton,
  useMediaQuery,
  Tooltip,
} from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import { Get, Post } from "@/utils/api";
import { useEffect, useState, useRef } from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import { toast } from "@/component/Show/Toast";
import { LoadingButton } from "@mui/lab";
import { MobileDatePicker } from "@mui/x-date-pickers";
import {
  Save,
  ArrowBack,
  Add,
  Delete,
  AccessTimeSharp,
} from "@mui/icons-material";
import { setLocalDate } from "@/utils/format";
import UploadImage from "@/component/UploadImage";
import { toBase64, base64Image } from "@/utils/formatFile";
import { Global } from "@/utils/global";

const KEY = "/organisasi/member";

const Member = observer(() => {
  const globalThis = new Global();
  const isMobile = useMediaQuery("(max-width:640px)");
  const meta = useLocalObservable(() => ({
    value: "",
    load: true,
    quickAccess: false,
  }));
  const [columns, setColumns] = useState([]);
  const [rows, setRows] = useState([]);

  const form = useRef(null);
  const del = useRef(null);

  const quickAccess = action(() => {
    meta.quickAccess = globalThis.quickAccess(KEY);
  });

  useEffect(() => {
    const fetch = async () => {
      try {
        const member = await Get("/member");
        console.log(member);
        if (member) {
          let columns = [
            {
              field: "nama",
              headerName: "Nama",
              minWidth: 300,
            },
            {
              field: "npm",
              headerName: "NPM",
              minWidth: 300,
            },
            {
              field: "Jabatan",
              headerName: "Jabatan",
              minWidth: 300,
              valueGetter: (params) => {
                return params.value.nama;
              },
            },
            {
              field: "email",
              headerName: "Email",
              minWidth: 300,
            },
            {
              field: "alamat",
              headerName: "Alamat",
              minWidth: 300,
            },
            {
              field: "tempatlahir",
              headerName: "Tempat Lahir",
              minWidth: 300,
            },
            {
              field: "tanggallahir",
              headerName: "Tanggal Lahir",
              minWidth: 300,
            },
            {
              field: "nohp",
              headerName: "No HP/Whatsapp",
              minWidth: 300,
            },
            {
              field: "otw_nama",
              headerName: "Nama Orang Tua/Wali",
              minWidth: 300,
            },
            {
              field: "otw_nohp",
              headerName: "No Hp Orang Tua/Wali",
              minWidth: 300,
            },
            {
              field: "status_alumni",
              headerName: "Status Alumni",
              minWidth: 300,
              valueGetter: (params) =>
                params.value == 0 ? "Member" : "Alumni",
            },
            {
              field: "status_pelatih",
              headerName: "Status Pelatih",
              minWidth: 300,
              valueGetter: (params) =>
                params.value == 0 ? "Siswa" : "Pelatih",
            },
            {
              field: "valid",
              headerName: "Status ",
              minWidth: 300,
              valueGetter: (params) =>
                params.value == 0 ? "Belum Valid" : "Valid",
            },
          ];

          setColumns([...columns]);
          setRows([...member.data]);

          runInAction(() => {
            meta.load = false;
          });
        }
      } catch (e) {
        runInAction(() => {
          meta.load = false;
        });
      }
    };

    runInAction(() => {
      meta.quickAccess = globalThis.checkQuickAccess(KEY);
    });

    fetch();
  }, [meta.value]);

  return (
    <Box sx={{ height: "90vh", display: "flex", flexDirection: "column" }}>
      <Box
        sx={{
          width: "100%",
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <IconButton
          aria-label="back"
          onClick={action(() => (meta.value = ""))}
          sx={{ display: !meta.value && "none" }}
        >
          <ArrowBack />
        </IconButton>
        <Typography variant="h5" sx={{ my: 2 }}>
          Member
        </Typography>
        <Tooltip title={`Quick Access ${meta.quickAccess ? "On" : "Off"}`}>
          <IconButton
            onClick={quickAccess}
            sx={{ display: meta.value && "none" }}
          >
            <AccessTimeSharp
              color={meta.quickAccess ? "primary" : ""}
              fontSize="small"
            />
          </IconButton>
        </Tooltip>
        {meta.value ? (
          <Box
            sx={{
              display: "flex",
              flex: 1,
              flexDirection: "row",
              justifyContent: "flex-end",
            }}
          >
            <LoadingButton
              variant="contained"
              color="success"
              size="medium"
              sx={{ my: 2, px: 4, mr: 1 }}
              startIcon={<Save />}
              loading={globalThis.saving}
              loadingPosition="start"
              onClick={() => {
                globalThis.saving = true;
                form.current && form.current.click();
              }}
            >
              {isMobile ? "" : "Simpan"}
            </LoadingButton>
            {meta.value.type !== "new" && (
              <Button
                variant="contained"
                color="error"
                size="medium"
                sx={{ my: 2, px: 4, mr: 1 }}
                type="button"
                startIcon={<Delete />}
                onClick={() => {
                  del.current && del.current.click();
                  meta.value = "";
                }}
              >
                {isMobile ? "" : "Hapus"}
              </Button>
            )}
          </Box>
        ) : (
          <Box
            sx={{
              display: "flex",
              flex: 1,
              flexDirection: "row",
              justifyContent: "flex-end",
            }}
          >
            <Button
              variant="contained"
              color="success"
              size="medium"
              sx={{ my: 2, px: 4, mr: 1 }}
              startIcon={<Add />}
              onClick={action(
                () =>
                  (meta.value = {
                    type: "new",
                  })
              )}
            >
              {isMobile ? "" : "Baru"}
            </Button>
          </Box>
        )}
      </Box>
      {!meta.value ? (
        <DataGrid
          rows={rows}
          columns={columns}
          loading={meta.load}
          onCellClick={action(
            (params) => (meta.value = { type: "update", ...params.row })
          )}
        />
      ) : (
        <FormField data={meta.value} formRef={{ form, del }} />
      )}
    </Box>
  );
});

export default Member;

const FormField = observer(({ data, formRef }) => {
  const meta = useLocalObservable(() => ({
    jabatan: [],
    image: data.foto || "",
  }));
  const validationSchema = yup.object({
    email: yup.string().email().required("Mohon diisi"),
    nama: yup.string().required("Mohon diisi"),
    npm: yup.number().required("Mohon diisi"),
    tempatlahir: yup.string().required("Mohon diisi"),
    tanggallahir: yup.string().required("Mohon diisi"),
    alamat: yup.string().required("Mohon diisi"),
    nohp: yup.string().required("Mohon diisi"),
    jabatan_id: yup.number().required("Mohon diisi"),
  });

  const formik = useFormik({
    initialValues: {
      email: data.email || "",
      nama: data.nama || "",
      npm: data.npm || "",
      tempatlahir: data.tempatlahir || "",
      tanggallahir: data.tanggallahir
        ? new Date(data.tanggallahir)
        : new Date(),
      alamat: data.alamat || "",
      nohp: data.nohp || "",
      jabatan_id: data.jabatan_id || 0,
      otw_nama: data.otw_nama || "",
      otw_nohp: data.otw_nohp || "",
      status_alumni: data.status_alumni || 0,
      status_pelatih: data.status_pelatih || 0,
      valid: data.status_valid || 0,
    },
    validationSchema: validationSchema,
    onSubmit: async (values) => {
      globalThis.saving = true;
      // let img = meta.image.base64 ? meta.image.base64 : "";
      await submitEvent(data.type, {
        ...values,
        // foto: img,
        tanggallahir: setLocalDate(values.tanggallahir),
      });
    },
  });

  const createNew = async (value) => {
    const cn = await Post("/register_member", {
      ...value,
      Jabatan: {
        connect: {
          id: value.jabatan_id,
        },
      },
    });

    if (cn) {
      toast.show("Berhasil", "Input Data Berhasil", "SUCCESS");
    } else {
      toast.show("Gagal", "Input Data Gagal", "ERROR");
    }
  };

  const updateData = async (value) => {
    const cn = await Post("/member/update", {
      id: data.id,
      ...value,
      Jabatan: {
        connect: {
          id: value.jabatan_id,
        },
      },
    });

    if (cn) {
      toast.show("Berhasil", "Update Data Berhasil", "SUCCESS");
    } else {
      toast.show("Gagal", "Update Data Gagal", "ERROR");
    }
  };

  const deleteData = async () => {
    const cn = await Post("/member/delete", {
      id: data.id,
    });

    if (cn) {
      toast.show("Berhasil", "Hapus Data Berhasil", "SUCCESS");
    } else {
      toast.show("Gagal", "Hapus Data Gagal", "ERROR");
    }
  };

  const submitEvent = async (type, value) => {
    if (type == "new") {
      await createNew(value);
    } else if (type == "update") {
      await updateData(value);
    }
    globalThis.saving = false;
  };

  // const setFileToUpload = action((e) => {
  //   const file = e.target.files[0];
  //   if (file) {
  //     const type = file.type != "" ? file.type : null;
  //     const fmtFile = type.split("/");
  //     if (["png", "jpg", "jpeg"].includes(fmtFile[1])) {
  //       toBase64(file).then(
  //         action((result) => {
  //           let respon = result.split(",");
  //           meta.image = {
  //             file: respon && respon[1] ? base64Image(type, respon[1]) : "",
  //             base64: respon[1],
  //           };
  //         })
  //       );
  //     } else {
  //       toast.show("Gagal", "Format file tidak didukung", "ERROR");
  //     }
  //   }
  // });

  useEffect(() => {
    const fetch = async () => {
      const jabatan = await Get("/jabatan");
      if (jabatan) {
        runInAction(() => {
          meta.jabatan = jabatan.data;
        });
      }
    };

    fetch();
  }, []);

  return (
    <Box
      sx={{
        width: "100%",
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
      }}
      component="form"
      onSubmit={formik.handleSubmit}
    >
      <TextField
        label="Nama member *"
        name="nama"
        variant="outlined"
        color="primary"
        fullWidth
        value={formik.values.nama}
        onChange={formik.handleChange}
        error={formik.touched.nama && Boolean(formik.errors.nama)}
        helperText={formik.touched.nama && formik.errors.nama}
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
      />
      <TextField
        label="Email *"
        name="email"
        variant="outlined"
        color="primary"
        fullWidth
        value={formik.values.email}
        onChange={formik.handleChange}
        error={formik.touched.email && Boolean(formik.errors.email)}
        helperText={formik.touched.email && formik.errors.email}
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
      />
      <TextField
        label="NPM *"
        name="npm"
        variant="outlined"
        color="primary"
        fullWidth
        value={formik.values.npm}
        onChange={formik.handleChange}
        error={formik.touched.npm && Boolean(formik.errors.npm)}
        helperText={formik.touched.npm && formik.errors.npm}
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
      />
      <TextField
        label="Tempat Lahir *"
        name="tempatlahir"
        variant="outlined"
        color="primary"
        fullWidth
        value={formik.values.tempatlahir}
        onChange={formik.handleChange}
        error={formik.touched.tempatlahir && Boolean(formik.errors.tempatlahir)}
        helperText={formik.touched.tempatlahir && formik.errors.tempatlahir}
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
      />
      <MobileDatePicker
        label="Tanggal Lahir *"
        inputFormat="MM/dd/yyyy"
        value={formik.values.tanggallahir}
        onChange={(value) => {
          formik.setFieldValue("tanggallahir", value);
        }}
        fullWidth
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
        color="primary"
      />
      <TextField
        label="Alamat *"
        name="alamat"
        variant="outlined"
        color="primary"
        fullWidth
        value={formik.values.alamat}
        onChange={formik.handleChange}
        error={formik.touched.alamat && Boolean(formik.errors.alamat)}
        helperText={formik.touched.alamat && formik.errors.alamat}
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
      />
      <TextField
        label="No HP/Whatsapp *"
        name="nohp"
        variant="outlined"
        color="primary"
        fullWidth
        value={formik.values.nohp}
        onChange={formik.handleChange}
        error={formik.touched.nohp && Boolean(formik.errors.nohp)}
        helperText={formik.touched.nohp && formik.errors.nohp}
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
      />
      <FormControl
        fullWidth
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
        error={formik.touched.jabatan_id && Boolean(formik.errors.jabatan_id)}
      >
        <InputLabel id="demo-simple-select-helper-label">Jabatan *</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          label="Jabatan *"
          name="jabatan_id"
          value={formik.values.jabatan_id}
          onChange={formik.handleChange}
          color="primary"
        >
          {meta.jabatan.map((item) => (
            <MenuItem key={item.id} value={item.id}>
              {item.nama}
            </MenuItem>
          ))}
        </Select>
        <FormHelperText>
          {formik.touched.jabatan_id && formik.errors.jabatan_id}
        </FormHelperText>
      </FormControl>
      <TextField
        label="Nama Orang Tua"
        name="otw_nama"
        variant="outlined"
        color="primary"
        fullWidth
        value={formik.values.otw_nama}
        onChange={formik.handleChange}
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
      />
      <TextField
        label="No Hp/Whatsapp Orang Tua"
        name="otw_nohp"
        variant="outlined"
        color="primary"
        fullWidth
        value={formik.values.otw_hp}
        onChange={formik.handleChange}
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
      />
      <FormControl
        fullWidth
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
      >
        <InputLabel id="demo-simple-select-helper-label">
          Status Alumni
        </InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          label="Status Alumni"
          name="status_alumni"
          value={formik.values.status_alumni}
          onChange={formik.handleChange}
          color="primary"
        >
          <MenuItem value={0}>Member</MenuItem>
          <MenuItem value={1}>Alumni</MenuItem>
        </Select>
      </FormControl>
      <FormControl
        fullWidth
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
      >
        <InputLabel id="demo-simple-select-helper-label">
          Status Pelatih
        </InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          label="Status Pelatih"
          name="status_pelatih"
          value={formik.values.status_pelatih}
          onChange={formik.handleChange}
          color="primary"
        >
          <MenuItem value={0}>Siswa</MenuItem>
          <MenuItem value={1}>Pelatih</MenuItem>
        </Select>
      </FormControl>
      <FormControl
        fullWidth
        sx={{ my: 1, mr: 1, width: { laptop: "45%", mobile: "90%" } }}
      >
        <InputLabel id="demo-simple-select-helper-label">Status</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          label="Status"
          name="valid"
          value={formik.values.valid}
          onChange={formik.handleChange}
          color="primary"
        >
          <MenuItem value={0}>Belum Valid</MenuItem>
          <MenuItem value={1}>Valid</MenuItem>
        </Select>
      </FormControl>
      {/* <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          my: 2,
        }}
      >
        <Typography variant="h6">Foto</Typography>
        <UploadImage src={meta.image} onChange={setFileToUpload} />
      </Box> */}
      <Button sx={{ display: "none" }} type="submit" ref={formRef.form} />
      <Button sx={{ display: "none" }} ref={formRef.del} onClick={deleteData} />
    </Box>
  );
});
