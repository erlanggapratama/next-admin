"use client";
import { observer, useLocalObservable } from "mobx-react-lite";
import { action } from "mobx";
import {
  Drawer,
  Box,
  IconButton,
  CssBaseline,
  Toolbar,
  useMediaQuery,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import ListMenu from "@/component/ListMenu";
import { useEffect } from "react";
import store from "@/utils/store";
import { useRouter } from "next/navigation";

const Dashboard = observer(({ children }) => {
  const router = useRouter();
  const isMobile = useMediaQuery("(max-width:640px)");
  const meta = useLocalObservable(() => ({
    openDrawer: false,
    container:
      typeof window !== "undefined" ? () => window.document.body : undefined,
  }));

  useEffect(() => {
    if (!store.view("session")) {
      router.replace("/");
    }
  }, []);

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <Box
        sx={{
          position: "fixed",
          width: { tablet: "calc(100% - 240px)" },
          ml: { tablet: "240px" },
        }}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={action(() => (meta.openDrawer = true))}
            sx={{ mr: 2, display: { tablet: "none" } }}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </Box>
      <Box
        component="nav"
        sx={{ width: { tablet: 240 }, flexShrink: { tablet: 0 } }}
      >
        <Drawer
          container={meta.container}
          variant="temporary"
          open={meta.openDrawer}
          onClose={action(() => (meta.openDrawer = false))}
          ModalProps={{
            keepMounted: true,
          }}
          sx={{
            display: { mobile: "block", tablet: "none" },
            "& .MuiDrawer-paper": { boxSizing: "border-box", width: 240 },
          }}
        >
          <ListMenu closeDrawer={action(() => (meta.openDrawer = false))} />
        </Drawer>
        <Drawer
          variant="permanent"
          sx={{
            display: { mobile: "none", tablet: "block" },
            "& .MuiDrawer-paper": { boxSizing: "border-box", width: 240 },
          }}
          open
        >
          <ListMenu closeDrawer={() => null} />
        </Drawer>
      </Box>
      <Box component="main" sx={{ flexGrow: 1, p: 3, maxWidth: "83%" }}>
        {isMobile && <Toolbar />}
        {children}
      </Box>
    </Box>
  );
});

export default Dashboard;
