"use client";
import { observer } from "mobx-react-lite";
import { Box, Typography, Divider, Button } from "@mui/material";
import { useRouter } from "next/navigation";
import Store from "@/utils/store";

const Home = observer(() => {
  const router = useRouter();
  const quick = Store.view("Quick");
  const session = Store.view("session");
  return (
    <Box sx={{ display: "flex", flexDirection: "column", flex: 1 }}>
      <Box sx={{ display: "flex", flexDirection: "row" }}>
        <Typography variant="h6" sx={{ my: 2 }}>
          Halo {session.username}, {session.title}
        </Typography>
      </Box>
      <Divider />
      <Typography variant="body2" sx={{ mt: 2, fontWeight: "lighter" }}>
        Quick Access
      </Typography>
      <Box
        flexWrap="wrap"
        sx={{ display: "flex", flexDirection: "row", my: 2, overflowX: "auto" }}
      >
        {Array.isArray(quick) &&
          quick.map((item, index) => {
            const str = item.split("/");
            return (
              <Button
                key={index}
                onClick={() => router.push("/dashboard" + item)}
              >
                {`${str[1]} ${str[2] || ""}`}
              </Button>
            );
          })}
      </Box>
    </Box>
  );
});

export default Home;
