import "./globals.css";
import ThemeRegistry from "../component/ThemeRegistry";

export const metadata = {
  title: "Next Admin",
  description: "Welcome to Next Admin",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body>
        <ThemeRegistry options={{ key: "mui" }}>{children}</ThemeRegistry>
      </body>
    </html>
  );
}
