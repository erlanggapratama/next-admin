"use client";
import { observer } from "mobx-react-lite";
import { useFormik } from "formik";
import { Post } from "@/utils/api";
import * as yup from "yup";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import LoadingButton from "@mui/lab/LoadingButton";
import { useState, useEffect } from "react";
import store from "@/utils/store";
import { useRouter } from "next/navigation";
import Store from "@/utils/store";
import { toast } from "@/component/Show/Toast";
import Image from "next/image";

const Login = observer(() => {
  const router = useRouter();
  const [login, setLogin] = useState(false);

  const validationSchema = yup.object({
    username: yup.string().required("Mohon diisi"),
    password: yup.string().required("Mohon diisi"),
  });

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema,
    onSubmit: async (value) => {
      setLogin(true);
      const login = await Post("/login_user", value);
      if (login.message === "success") {
        Store.set("session", login.data);
        setLogin(false);
        router.replace("/dashboard");
      } else {
        toast.show("Gagal", login.message, "ERROR");
      }
      setLogin(false);
    },
  });

  useEffect(() => {
    if (store.view("session")) {
      router.replace("/dashboard");
    }
  }, []);

  return (
    <Box
      sx={{
        bgcolor: "bg.main",
        textAlign: "center",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        p: 2,
        height: "100vh",
      }}
    >
      <Image
        src="/assets/image/favicon.png"
        alt="logo"
        width={120}
        height={120}
      />
      <Typography variant="h4" sx={{ my: 2 }}>
        Next Admin
      </Typography>
      <Box sx={{ mt: 2 }} component="form" onSubmit={formik.handleSubmit}>
        <TextField
          label="Username"
          name="username"
          variant="outlined"
          color="primary"
          fullWidth
          value={formik.values.username}
          onChange={formik.handleChange}
          error={formik.touched.username && Boolean(formik.errors.username)}
          helperText={formik.touched.username && formik.errors.username}
          sx={{ mb: 1 }}
        />
        <TextField
          label="Password"
          type="password"
          name="password"
          color="primary"
          fullWidth
          value={formik.values.password}
          onChange={formik.handleChange}
          error={formik.touched.password && Boolean(formik.errors.password)}
          helperText={formik.touched.password && formik.errors.password}
          sx={{ my: 1 }}
        />
        <LoadingButton
          variant="contained"
          color="primary"
          size="large"
          loading={login}
          sx={{ mt: 5, px: 10, mb: 5 }}
          type="submit"
        >
          Login
        </LoadingButton>
      </Box>
    </Box>
  );
});

export default Login;
