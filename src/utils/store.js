import { observable } from "mobx";
import { createContext } from "react";

const saveLocalStorage = (key, value) => {
  if (typeof window !== "undefined") {
    localStorage.setItem(key, JSON.stringify(value));
  }
};

const getLocalStorage = (key) => {
  if (typeof window !== "undefined") {
    const getItem = localStorage.getItem(key);
    return JSON.parse(getItem);
  }

  return null;
};

const removeLocalStorage = (key) => {
  if (typeof window !== "undefined") {
    localStorage.removeItem(key);
  }
};

const clearLocalStorage = () => {
  if (typeof window !== undefined) {
    localStorage.clear();
  }
};

const Store = observable({
  setStore(key, data) {
    saveLocalStorage(key, JSON.stringify(data));
  },
  remove(key) {
    removeLocalStorage(key);
  },
  clear() {
    clearLocalStorage();
  },
  checkStore(key) {
    if (getLocalStorage(key)) return true;
  },
  view(key) {
    if (this.checkStore(key)) {
      const item = getLocalStorage(key);
      if (item) return item;
    }
    return "";
  },
  set(key, data) {
    if (!data) return;

    this.setStore(key, data);
  },
  push(key, data) {
    const check = this.view(key);
    let newArr = [];
    if (check) {
      if (Array.isArray(check)) {
        newArr = [...check, data];
      } else {
        newArr = [check, data];
      }
    } else {
      newArr = [data];
    }

    this.setStore(key, newArr);
  },
});

export default Store;
export const StoreContext = createContext(Store);
