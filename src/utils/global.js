import { observable, action, makeObservable } from "mobx";
import Store from "./store";

export class Global {
  saving = false;
  removing = false;

  constructor() {
    makeObservable(this, {
      saving: observable,
      removing: observable,
      quickAccess: action,
      checkQuickAccess: action,
    });
  }

  quickAccess(Key) {
    const checkQuick = Store.view("Quick");
    if (checkQuick) {
      const exist = checkQuick.find((x) => x === Key);
      if (exist) {
        const filterit = checkQuick.filter((x) => x !== Key);
        Store.set("Quick", filterit);
        return false;
      } else {
        Store.push("Quick", Key);
        return true;
      }
    } else {
      Store.push("Quick", Key);
      return true;
    }
  }

  checkQuickAccess(Key) {
    const checkQuick = Store.view("Quick");
    if (checkQuick) {
      const exist = checkQuick.find((x) => x === Key);
      if (exist) {
        return true;
      } else {
        return false;
      }
    }
  }
}
