import * as Axios from "axios";

const prefix = "/api";

export const Get = async (url) => {
  const send = await Axios(prefix + url);

  if (send.data) return send.data;
  return send;
};

export const Post = async (url, data) => {
  const send = await Axios({
    url: prefix + url,
    method: "POST",
    data,
  });

  if (send.data) return send.data;

  return send;
};
