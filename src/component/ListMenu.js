import { observer, useLocalObservable } from "mobx-react-lite";
import { runInAction } from "mobx";
import {
  List,
  ListItem,
  ListItemText,
  Typography,
  Box,
  Collapse,
} from "@mui/material";
import { ExpandLess, ExpandMore } from "@mui/icons-material/";
import { useRouter } from "next/navigation";
import Store from "@/utils/store";
import { useEffect } from "react";

export const menu = [
  {
    name: "Dashboard",
    path: "/",
  },
  // {
  //   name: "Blog",
  //   path: "/Blog",
  // },
  {
    name: "Organisasi",
    path: "/organisasi",
    child: [
      {
        name: "Member",
        path: "/member",
      },
      // {
      //   name: "Kepengurusan",
      //   path: "/Kepengurusan",
      // },
    ],
  },
  {
    name: "Master",
    path: "/master",
    child: [
      // {
      //   name: "Fakultas",
      //   path: "/Fakultas",
      // },
      // {
      //   name: "Jurusan",
      //   path: "/Jurusan",
      // },
      {
        name: "Jabatan",
        path: "/jabatan",
      },
    ],
  },
  {
    name: "Setting",
    path: "/setting",
    child: [
      {
        name: "User",
        path: "/user",
      },
    ],
  },
];

export default observer(({ closeDrawer }) => {
  const router = useRouter();
  const meta = useLocalObservable(() => ({
    open: {},
  }));
  useEffect(() => {
    menu.map((item) => {
      if (item.child) {
        runInAction(() => {
          meta.open[item.name] = false;
        });
      }
    });
  }, []);

  return (
    <List>
      <Box sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
        <img src="/assets/image/favicon.png" alt="logo" width={60} />
        <Typography variant="h5">Next Admin</Typography>
      </Box>
      {menu.map((item, index) => {
        if (item.child) {
          return (
            <Box key={index}>
              <ListItem
                button
                onClick={() => {
                  runInAction(() => {
                    meta.open[item.name] = !meta.open[item.name];
                  });
                }}
              >
                <ListItemText primary={item.name} />
                {meta.open[item.name] ? <ExpandLess /> : <ExpandMore />}
              </ListItem>
              <Collapse in={meta.open[item.name]} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  {item.child.map((val, idx) => (
                    <ListItem
                      key={idx}
                      sx={{ pl: 4 }}
                      button
                      onClick={() => {
                        router.push("/dashboard" + item.path + val.path);
                        closeDrawer();
                      }}
                    >
                      <ListItemText primary={val.name} />
                    </ListItem>
                  ))}
                </List>
              </Collapse>
            </Box>
          );
        } else {
          return (
            <ListItem
              key={index}
              button
              onClick={() => {
                router.push("/dashboard" + item.path);
                closeDrawer();
              }}
            >
              <ListItemText primary={item.name} />
            </ListItem>
          );
        }
      })}
      <ListItem
        button
        onClick={() => {
          Store.remove("session");
          router.replace("/");
        }}
      >
        <ListItemText>Logout</ListItemText>
      </ListItem>
    </List>
  );
});
